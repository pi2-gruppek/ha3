## Aufgabe 1

# Frage 4

1. Warum ist die gemeinsame Nutzung des Busses fur Daten und Befehle ein Nachteil?

Die gemeinsame Nutzung des Busses für Daten und Befehle hat folgende Nachteile:

- **Bus-Kollisionen und Wartezeiten:** Daten und Befehle können nicht gleichzeitig übertragen werden, was zu Verzögerungen führt.
- **Geschwindigkeitsbegrenzungen:** Die Bandbreite des Busses wird geteilt, was die Systemgeschwindigkeit reduziert.
- **Kompromisse bei der Architektur:** Gemeinsamer Speicher und Bus erfordern Kompromisse in der Speicherverwaltung und Cache-Effizienz.
- **Engpässe bei der Bandbreite:** Hohe Daten- oder Befehlsmengen können den Bus überlasten und Leistungseinbußen verursachen.
- **Komplexere Steuerlogik:** Die Verwaltung beider Zugriffe macht die Steuerlogik komplizierter und erhöht die Systemkosten.

Diese Probleme können durch separate Busse für Daten und Befehle, wie in der Harvard-Architektur, vermieden werden.

2. Mit welchen Befehlen ist es möglich (bedingt und/oder unbedingt) an unterschiedliche
   Stellen im Maschinencode zu springen? Welche Kontrollstrukturen kann man damit beispielsweise umsetzen?

- JMP: Springe an eine spezifische Adresse, unabhängig von einer Bedingung. 
- JZ: Sprung, wenn log. Ergebnis gleich 0 ist.
- JNZ: Sprung, wenn log. Ergebnis ungleich 0 ist.
- JNG: Sprung wenn Ergebnis einer log. Operation nicht arith. größer ist.
- JE : Sprung, wenn zwei Operanten gleich sind.
- JG : Sprung, wenn erster Operant arith. größer ist, als der zweite Operant.
- JL : Sprung, wenn erster Operant arith. kleiner ist, als der zweite Operant.

Kontrollstrukturen: 
1. If-Anweisungen (z.B. durch JE, JZ)
2. For-Schleife/While-Schleife (z.B durch JMP, JE)
3. Switch/Case-Anweisungen (z.B durch JMP, JE)
