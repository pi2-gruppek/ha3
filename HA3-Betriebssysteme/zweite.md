# Aufgabe 2

## Frage 4

1. Beschreiben Sie stichpunktartig vier verschiedene Zustände, die ein Prozess in einem Betriebssystem haben kann.
    - **running**     (Prozess wird gerade ausgeführt)    
    - **exit**        (Prozess wird beendet)
    - **new**         (Neuer Prozess)
    - **ready**       (Prozess bereit, ausgeführt zu werden)


2. Erklären Sie kurz den Unterschied zwischen User Mode und Kernel Mode.

    Der Kernel Mode besitzt kompletten Zugriff auf die Hardware und dient als zusätzliche Abstraktionsschicht. Apps im User Mode müssen für den Zugriff auf z.B. das Dateisystem einen Systemaufruf starten, welcher im Kernel abgearbeitet wird.

    Der Kernel dient zusätzlich der Zuverlässigkeit, indem er entscheiden kann, wie lange eine App auf eine bestimmte Hardwareressource zugreifen darf.

3. Mit welchem Befehl lasse ich mir die derzeit vom Benutzer ausgefuhrten Prozesse anzeigen?
    - ps (Alle Prozesse des Benutzers)
    - ps ax (Alle Prozesse aller Benutzer)

4. Sie versuchen eine Datei auszuführen (beispielsweise mvnw), bekommen aber eine Fehlermeldung, dass Sie diese wegen unzureichender Berechtigungen nicht ausfuhren können. Mit
welchem UNIX-Befehl können Sie Berechtigungen zur Ausführung dieser Datei erteilen? 
Geben Sie eine Antwort sowohl in symbolischer als auch in oktaler Form!
    - Symbolische Form: chmod u+x mvnw

    - Oktale Form: chmod 744 mvnw